### NOTE: Make sure that `composer` is installed in your machine

## Installation
1. Clone the repository to you local machine.
2. Open terminal and go to root folder of the project, run `git fetch` then `git checkout develop`.
2. Copy .env.example and rename it to .env
3. Go to the root directory of the project and open .env file and change the database variables based on your settings
   For example:
		DB_CONNECTION=mysql
		DB_HOST=127.0.0.1
		DB_PORT=3306
		DB_DATABASE=adgroup-exam
		DB_USERNAME=root
		DB_PASSWORD=root
4. Run `composer install` to install the vendor packages.
5. Open your `http://localhost/phpmyadmin` and create a new database. For example: `adgroup-exam`.
6. Run `php artisan key:generate` to generate the app key.
7. Run `php artisan passport:install` to setup Laravel passport
8. Run `php artisan migrate` to setup the database.
9. Run `php artisan serve`. The  default API url is http://localhost:8000.

**API is now running. You can now setup the frontend.**

## How it works

This is for the API that connects the frontend to database. The project is following the Repository pattern where we do the CRUD operations and just let the controllers to call them.

Activity logs are being saved into the database.

User should have a valid token to access the data. If not, it will return `Unauthorized` message.